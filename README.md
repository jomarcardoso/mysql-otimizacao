# Otimizador MySQL

## Introdução

Otimização envolve configuração, ajuste e medição de desempenho, em vários níveis. As vezes, você pode ser 
proativo e planejar antecipadamente o desempenho, enquanto outras vezes você pode solucionar um problema de
configuração ou código depois que um problema ocorrer. A otimização do uso da CPU e da memória também pode
melhorar a escalabilidade, permitindo que o banco de dados manipule mais carga sem diminuir a velocidade.

A documentação do MySQL não fala muito de seu otimizador e como funciona, contém mais sobre técnicas 
para otimizá-lo.

Sobre os tipos de tabelas existem innoDB, MyISAM e MEMORY, mas neste abordarei apenas sobre a InnoDB que é 
mais comum.

## Visão geral de otimização

O desempenho do banco de dados depende de vários fatores no nível do banco de dados, como tabelas, 
consultas e definições de configuração. Essas construções de software resultam em operações de CPU 
e I/O no nível de hardware, que você deve minimizar e tornar o mais eficiente possível. 

Usuários típicos buscam obter o melhor desempenho de banco de dados de suas configurações de software 
e hardware existentes. Os usuários avançados buscam oportunidades para melhorar o próprio software
MySQL ou desenvolvem seus próprios mecanismos de armazenamento e dispositivos de hardware para 
expandir o ecossistema do MySQL.

- Otimizando no nível do banco de dados
- Otimizando no nível de hardware

### Otimizando no nível do banco de dados

O fator mais importante para tornar um aplicativo de banco de dados rápido é o seu design básico:

- As tabelas estão estruturadas corretamente? Em particular, as colunas têm os tipos de dados corretos e cada tabela possui as colunas apropriadas para o tipo de trabalho?
- Os índices corretos estão prontos para tornar as consultas eficientes?
- Cada tabela usa um formato de linha apropriado? Em particular, as tabelas compactadas usam menos espaço em disco e, portanto, exigem menos I/O de disco para ler e gravar os dados. 
- Todas as áreas de memória usadas para armazenamento em cache estão dimensionadas corretamente? Ou seja, grande o suficiente para armazenar os dados acessados com freqüência, mas não tão grande que sobrecarregue a memória física e cause paginação.

### Otimizando no nível de hardware

Qualquer aplicativo de banco de dados eventualmente atinge os limites de hardware à medida que o banco de dados
se torna mais e mais ocupado. Um DBA deve avaliar se é possível ajustar o aplicativo ou reconfigurar o servidor
para evitar esses gargalos ou se são necessários mais recursos de hardware. Os afunilamentos do sistema
geralmente surgem dessas fontes:

- Procura de disco. Leva tempo para o disco encontrar um pedaço de dados. Desta vez, melhora lentamente com novos discos e é muito difícil de otimizar para uma única tabela. A maneira de otimizar o tempo de busca é distribuir os dados em mais de um disco.
- Leitura e gravação de disco. Quando o disco está na posição correta, precisamos ler ou gravar os dados. Com discos modernos, um disco fornece pelo menos 10 a 20MB / s de throughput. Isso é mais fácil de otimizar do que as pesquisas, porque você pode ler em paralelo a partir de vários discos.
- Ciclos de CPU. Quando os dados estão na memória principal, devemos processá-los para obter nosso resultado. Ter grandes tabelas comparadas com a quantidade de memória é o fator limitante mais comum.
- Largura de banda de memória. Quando a CPU precisa de mais dados do que pode caber no cache da CPU, a largura de banda da memória principal se torna um gargalo.

## Otimizador MySQl

O otimizador é o conjunto de rotinas que decidem qual caminho de execução o SGBD deve tomar para as consultas.

Uma transformação ocorre quando uma consulta é alterada para outra consulta que fornece o mesmo resultado. Por exemplo:

```
SELECT ... WHERE 5 = a
```
```
SELECT ...WHERE a = 5
```

### Código do otimizador

Este diagrama mostra a estrutura da função handle_select() em /sql/sql_select.cc:

```
handle_select()
   mysql_select()
     JOIN::prepare()
       setup_fields()
     JOIN::optimize()            /* optimizer is from here ... */
       optimize_cond()
       opt_sum_query()
       make_join_statistics()
         get_quick_record_count()
         choose_plan()
           /* Find the best way to access tables */
           /* as specified by the user.          */
           optimize_straight_join()
             best_access_path()
           /* Find a (sub-)optimal plan among all or subset */
           /* of all possible query plans where the user    */
           /* controls the exhaustiveness of the search.   */
           greedy_search()
             best_extension_by_limited_search()
               best_access_path()
           /* Perform an exhaustive search for an optimal plan */
           find_best()
       make_join_select()        /* ... to here */
     JOIN::exec()
```

O indentação do código mostra o que chama o quê. O handle_select() chama mysql_select(),
que chama JOIN::prepare(), que chama setup_fields(), e assim por diante. 

- **IN::prepare** - A primeira parte do mysql_select() é JOIN::prepare(), que é para análise de contexto, configuração de metadados e algumas transformações de subconsultas.
- **IN::optimize** - O otimizador é JOIN :: optimize () e todas as suas rotinas subordinadas. 
- **JOIN::exec** - Quando o otimizador termina, JOIN::exec() assume e faz o trabalho que JOIN::optimize() decidiu.

**JOIN** aqui não é usado apenas em consultas com ligações de tabelas

- **optimize_cond()** e **opt_sum_query()** - executam as transformações. 
- **make_join_statistics()** - reúne todas as informações que pode encontrar os índices que podem ser úteis para acessar as tabelas da consulta.

### Otimizador: otimizando constantes

Transforma uma query assim:

```
WHERE column1 = column2 AND column2 = 'x'
```

em uma assim:

```
WHERE column1='x' AND column2='x'
```

### Otimizador: eliminando código não usado

Expressões sempre verdadeiras:

```
WHERE 0=0 OR column1='y'

```

Colunas não nulas:

```
WHERE not_null_column IS NULL
```

Ou condições impossíveis:

```
CREATE TABLE Table1 (column1 CHAR(1));
...
SELECT * FROM Table1 WHERE column1 = 'Canada';
```

### Otimizador: calculando constantes

```
WHERE column1 = 1 + 2
```

se torna

```
WHERE column1 = 3
```

### Otimizador: escolhendo o tipo do Join

O join aqui novamente é o tipo de acesso para a consulta.
O MySql escolhe o este conforme os dados da consulta.
Em ordem ordem do melhor para o pior:

- sytem: uma tabela de sistema que é uma tabela constante;
- const: uma tablema constante;
- eq_ref: um índice único ou primário com uma relação de igualdade;
- ref: um índice com uma relação de igualdade, em que o valor do índice não pode ser NULL;
- ref_or_null: um índice com uma relação de igualdade, em que é possível que o valor do índice seja NULL;
- range: um índice com uma relação como BETWEEN, IN,> =, LIKE e assim por diante.
- index: uma busca sequencial em um índice;
- ALL: uma busca sequencial da tabela inteira.

### Otimizador: ORDER BY

Se a coluna a ser ordenada tiver index será usado.

### Otimizador: GROUP BY

Este inclui todas as funções de grupo.

- também usa index se houver
- se não houver index o group by ordena os elementos
- se houver order by junto o otimizador perceberá que este é desnecessário

### Controlando o otimizador

A tarefa do otimizador de consulta é encontrar um plano ideal para executar uma consulta SQL e conforme o 
número de tabelas aumenta o tempo de otimizar pode se tornar um gargalo.

O MySQL fornece controle do otimizador por meio de variáveis do sistema que afetam como os planos de consulta 
são avaliados, otimizações comutáveis, dicas do otimizador e do índice e o modelo de custo do otimizador.

O comportamento do otimizador em relação ao número de planos que ele avalia pode ser controlado usando duas 
variáveis do sistema:

- A variável optimizer_prune_level informa ao otimizador para ignorar determinados planos com base nas estimativas do número de linhas acessadas para cada tabela. Nossa experiência mostra que esse tipo de “ suposição educada ” raramente perde os planos ideais e pode reduzir drasticamente os tempos de compilação de consultas. É por isso que esta opção está ativada ( optimizer_prune_level=1) por padrão.

- optimizer_search_depth 

### Dicas do otimizador

Uma maneira de controlar o otimizador é usando dicas do otimizador, que podem ser especificadas em instruções
individuais. A sintaxe é ```/*+ ... */```

## Otimizando instruções SQL

A lógica central de um aplicativo de banco de dados é executada por meio de instruções SQL e emitidas 
diretamente por um intérprete. Uma consulta em uma tabela enorme pode ser executada sem ler todas as linhas, 
uma união envolvendo várias tabelas pode ser executada sem comparar todas as combinações de linhas. O conjunto
de operações que o otimizador escolhe para executar a consulta mais eficiente é chamado de 
"plano de execução de consulta" ou EXPLAIN.

### Instrução SELECT

As principais considerações para otimizar as consultas são:

- Para fazer SELECT ... WHERE mais rápido, a primeira coisa a verificar é se você pode adicionar um índice. Configure índices nas colunas usadas na WHEREcláusula, para acelerar a avaliação, a filtragem e a recuperação final dos resultados. Configure índices nas colunas usadas na WHEREcláusula, para acelerar a avaliação, a filtragem e a recuperação final dos resultados.
- Os índices são especialmente importantes para consultas que fazem referência a tabelas diferentes, usando recursos como JOINS e Foreing Keys.
- Isole e ajuste qualquer parte da consulta. Evitar chamar funções demoradas na leitura de cada linha da tabela.
- Minimize o número de varreduras de tabela completas em suas consultas, principalmente para tabelas grandes.
- 

## Otimização de estrutura de banco de dados

Tabelas menores normalmente requerem menos memória principal enquanto seu conteúdo está sendo processado 
ativamente durante a execução da consulta. Qualquer redução de espaço para dados da tabela também resulta 
em índices menores que podem ser processados mais rapidamente.

O MySQL suporta muitos mecanismos de armazenamento diferentes (tipos de tabelas) e formatos de linhas.
Para cada tabela, você pode decidir qual método de armazenamento e indexação usar. 

### Colunas da tabela

Use os tipos de dados mais eficientes (menores) possíveis. O MySQL possui muitos tipos especializados que 
economizam espaço em disco e memória. Por exemplo, MEDIUMINT que é 25% menor que o INT.

Declare colunas para ser NOT NULL, se possível. Isso torna as operações SQL mais rápidas, permitindo 
um melhor uso dos índices e eliminando a sobrecarga para testar se cada valor é NULL.

**Tipo String

Ao comparar valores de diferentes colunas, declare essas colunas com o mesmo conjunto de caracteres e 
agrupamento sempre que possível, para evitar conversões de sequência enquanto executa a consulta.

Se uma tabela contiver colunas de string como nome e endereço, mas muitas consultas não recuperarem essas 
colunas, considere dividir as colunas de string em uma tabela separada e usar consultas de junção com uma 
chave estrangeira quando necessário, reduzindo assim o I/O.

Quando você usa um valor gerado aleatoriamente como uma chave primária em uma tabela, prefixe-o com um valor 
ascendente, como a data e a hora atuais, se possível. Quando valores primários consecutivos são fisicamente 
armazenados próximos uns dos outros, esses podem ser inseridos e recuperados mais rapidamente.

Use o tipo VARCHAR de dados em vez de CHAR para armazenar cadeias de comprimento variável ou colunas com muitos
valores nulos.

Para tabelas grandes, ou que contenham muitos dados repetitivos ou de texto numérico, considere usar o 
formato de linha COMPRESSED. Menos I/O de disco é necessário para trazer dados para o buffer ou para executar
varreduras de tabela completas.

### Joins

Em algumas circunstâncias, pode ser benéfico dividir em duas uma tabela que é escaneada com muita freqüência. 
Isso é especialmente verdadeiro se for uma tabela de formato dinâmico e for possível usar uma tabela de 
formato estático menor que possa ser usada para localizar as linhas relevantes ao examinar a tabela.

Declare colunas com informações idênticas em tabelas diferentes com tipos de dados idênticos, para acelerar 
junções com base nas colunas correspondentes.

Mantenha os nomes das colunas simples, para que você possa usar o mesmo nome em diferentes tabelas e 
simplificar as consultas de associação.

### Normalização

Se a velocidade for mais importante que o espaço em disco e os custos de manutenção de várias cópias de dados,
por exemplo, em um cenário de business intelligence em que você analisa todos os dados de tabelas grandes, 
relaxe as regras de normalização, duplique informações ou crie tabelas de resumo ganhe mais velocidade.

### Índices

Usar chaves primárias curtas diminui o tamanho de cada índice secundário criado.

Crie apenas os índices que você precisa para melhorar o desempenho da consulta. Os índices são bons para 
recuperação, mas diminuem as operações de inserção e atualização.

Se você acessar uma tabela principalmente pesquisando em uma combinação de colunas, crie um único índice 
composto sobre elas, em vez de um índice separado para cada coluna.  A primeira parte do índice deve ser a 
coluna mais usada. 

Dependendo dos detalhes de suas tabelas, colunas, índices e condições em sua cláusula WHERE, o otimizador 
do MySQL considera muitas técnicas para realizar eficientemente as pesquisas envolvidas em uma consulta SQL.

### Formato de linha

As tabelas são criadas usando o formato DYNAMIC de linha por padrão. 

Os tipos compactos são COMPACT, DYNAMIC E COMPRESSED, estes diminuem o espaço de armazenamento ao custo de 
aumentar o uso da CPU. Se a sua carga de trabalho for típica, limitada pelas taxas de acertos do cache e 
pela velocidade do disco, ela provavelmente será mais rápida. 

A família compacta de formatos de linha também otimiza o CHAR de colunas ao usar um conjunto de caracteres 
de comprimento variável.

Para minimizar ainda mais o espaço, armazenando os dados da tabela em formato compactado, especifique
ROW_FORMAT=COMPRESSED.

## Otimização e índices

A melhor maneira de melhorar o desempenho das instruções SELECT é criar índices em uma ou mais colunas testadas na consulta.

### Como o MySQL usa índices

A maior parte dos índices MySQL ( PRIMARY KEY, UNIQUE, INDEX, e FULLTEXT) são armazenados em B-trees. 
Exceções: Índices em tipos de dados espaciais usam R-trees; tabelas MEMORY também suportam índices de 
hash.

O MySQL usa índices para estas operações:

- Para encontrar as linhas que correspondem a uma cláusula WHERE mais rapidamente.
- Se houver uma escolha entre vários índices, o MySQL normalmente usa o índice que encontra o menor número de linhas.
- Se a tabela tiver um índice de várias colunas, qualquer prefixo mais à esquerda do índice poderá ser usado pelo otimizador para a busca. 
- Para recuperar linhas de outras tabelas ao executar JOINS. MySQL pode usar índices em colunas com mais eficiência se forem declarados como do mesmo tipo e tamanho.
- Para encontrar o valor mínimo ou máximo de uma coluna com index. MySQL verifica se o valor é uma constante e salva uma chave para ele.
- Se um ORDER BY for decrescente e de duas ou mais colunas, é lido estas em ordem reversa.
- Em alguns casos, uma consulta pode ser otimizada para recuperar valores sem consultar a tabela propriamente.

### Comparação dos índices B-Tree e Hash

**Características do índice B-Tree

Um índice de árvore-B pode ser utilizada para comparações de coluna em expressões que utilizam
os operadores =, >, >=, <, <=, BETWEEN ou LIKE (se não começar com %).

Qualquer índice que não abranja todos os AND na cláusula WHERE não é usado para otimizar a consulta.

A seguintes cláusulas WHERE usam índices:

```
... WHERE index_part1=1 AND index_part2=2 AND other_column=3

    /* index = 1 OR index = 2 */
... WHERE index=1 OR A=10 AND index=2

    /* optimized like "index_part1='hello'" */
... WHERE index_part1='hello' AND index_part3=5

    /* Can use index on index1 but not on index2 or index3 */
... WHERE index1=1 AND index2=2 OR index1=3 AND index3=3;
```

Essas não usam índices:

```
/* index_part1 is not used */
... WHERE index_part2=1 AND index_part3=2

    /*  Index is not used in both parts of the WHERE clause  */
... WHERE index=1 OR A=10

    /* No index spans all rows  */
... WHERE index_part1=1 OR index_part2=10
```

Às vezes, o MySQL não usa um índice, mesmo se estiver disponível. Uma circunstância sob a qual isso ocorre 
é quando o otimizador estima que usar o índice exigiria que o MySQL acessasse uma porcentagem muito grande 
das linhas na tabela. Entretanto, se essa consulta usa LIMIT para recuperar apenas algumas das linhas, o MySQL 
usa um índice de qualquer maneira, porque pode encontrar muito mais rapidamente algumas linhas para retornar no 
resultado.

### Características do índice de hash

Índices de hash têm características um pouco diferentes daquelas que acabamos de discutir:

- Eles são usados apenas para comparações de igualdade que usam os operadores =ou <=>. Os sistemas que dependem desse tipo de consulta de valor único são conhecidos como "Key-value stores".
- O otimizador não pode usar um índice de hash para operações ORDER BY, logo não pode ser usado para adicionar novos valores ao índice, pois desconhece a ordem.
- Apenas chaves inteiras podem ser usadas para procurar uma linha.



























